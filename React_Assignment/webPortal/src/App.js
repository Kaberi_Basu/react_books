import React from "react"
import Header from './components/Header'
import Main from './components/Main'
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom'

class App extends React.Component {
    constructor() {
        super();
        
    }
    
    render() {
        return(
            <div>
                <div>
                    <Header/>
                    <Main/>
                </div>
            </div>
        )
    }
}
export default App