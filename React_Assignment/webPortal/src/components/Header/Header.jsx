import React from "react"
import Tab from './Tab'

class Header extends React.Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-default">
                    <div className="container">
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul className="nav navbar-nav">
                                    <Tab/>
                                    <Tab/>
                                    <Tab/>
                                </ul>
                        </div>
                    </div>
                </nav>
                <div id="bar">
                    <img src="assets/img/bar.jpg" />
                </div>
            </div>
        )
    }
}

export default Header

