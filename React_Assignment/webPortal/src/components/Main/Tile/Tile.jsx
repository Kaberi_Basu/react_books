import React from "react"

export default class Tile extends React.Component {
    render() {
        return (
                <div className="col-md-4 col-sm-6">
                    <div className="thumbnail">
                        <img className="cover" src="assets/img/sample5.jpg" />
                        <img src="assets/img/user.png" className="user" />
                        <div className="hover">
                            <h3 className="text-center">BHAJAN</h3>
                            <br></br>
                        </div>
                    </div>
                    <div className="content bg-orange">
                        <h4>Lorem Ipsum</h4>
                        <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</small></p>
                        <button type="button" className="btn btn-default btn-round btn-round-animated" data-container="body" data-toggle="popover" data-placement="bottom" data-trigger="focus"><span className="glyphicon glyphicon-download-alt"></span></button>
                        &nbsp;&nbsp;
                        <button type="button" className="btn btn-default btn-round btn-round-animated" data-container="body" data-toggle="popover" data-placement="bottom" data-trigger="focus"><span className="glyphicon glyphicon-share"></span></button>
                    </div>
                </div>
        )
    }
}


