import React from "react"
import Tile from './Tile'
import apiCall from './api'

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentWillMount() {
        Promise.resolve(apiCall())
            .then((res) => {
                if (res && res.body && res.body.result)
                    console.log("res====",res);
            })
            .catch((err) => {
                console.log("err====",err);
            })
    }
    
    render() {
        return (
            <div>
                <section className="container bg-gray">
                    <div className="wraper">
                        <div className="row">
                            <div className="col-sm-3">
                                <button type="button" id="add_audio" className="btn btn-default btn-lg btn-violet"><span className="glyphicon glyphicon-music"></span> Add Audio</button>
                            </div>

                            <div className="col-sm-6 text-center">
                                <div className="btn-group btn-group-lg" role="group" aria-label="...">
                                    <button type="button" className="btn btn-default active">Music</button>
                                    <button type="button" className="btn btn-default">Shastra</button>
                                    <button type="button" className="btn btn-default">Dhyan Sarita</button>
                                </div>
                            </div>

                            <div className="col-sm-3 text-right">
                                <button type="button" id="video_demo" className="btn btn-default btn-lg btn-violet"><span className="glyphicon glyphicon-paste"></span> Add Shastra</button>
                            </div>

                        </div>

                        <div className="row" id="content">
                            <div className="col-md-4 col-sm-6">
                                <div className="thumbnail">
                                    <img className="cover" src="assets/img/sample5.jpg" />
                                        <img src="assets/img/user.png" className="user" />
                                            <div className="hover">
                                                <h3 className="text-center">BHAJAN</h3>
                                                <br></br>
                                            </div>
                                </div>
                                <div className="content bg-orange">
                                    <h4>Lorem Ipsum</h4>
                                    <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</small></p>
                                    <button type="button" className="btn btn-default btn-round btn-round-animated" data-container="body" data-toggle="popover" data-placement="bottom" data-trigger="focus"><span className="glyphicon glyphicon-download-alt"></span></button>
                                    &nbsp;&nbsp;
                                    <button type="button" className="btn btn-default btn-round btn-round-animated" data-container="body" data-toggle="popover" data-placement="bottom" data-trigger="focus"><span className="glyphicon glyphicon-share"></span></button>
                                </div>
                            </div>
                            <Tile/>
                            <Tile/>
                            <Tile/>
                            <Tile/>
                            <Tile/>


                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div id="round-nav" className="text-center">
                                    <a className="active" href="#"></a>
                                    <a href="#"></a>
                                    <a href="#"></a>
                                    <a href="#"></a>
                                    <a href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default Main

