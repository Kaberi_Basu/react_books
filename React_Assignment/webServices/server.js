var express = require('express');
var app = express();
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var handler = require('./helper/response');
var flash = require('connect-flash');
var configModule = require('config');
var cors = require('cors');

var appConfig = configModule.get('Customer.appConfig');



//mongoose.Promise = global.Promise;
//mongoose.connect("mongodb://localhost" + "/" + appConfig.dbName,{
//    useMongoClient: true
//});
//
//
var port = appConfig.backEndPort || 8080;
//var db = mongoose.connection;
//db.on('error', function (error) {
//    console.log("Database not connected ", error);
//});
//db.once('open', function () {
//    console.log("Database connected");
//});

app.use(logger('combined'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(flash());

require('./routes')(app);
process.on('uncaughtException', function (err) {
    console.log("uncaughtException", err);
});
//
// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
//     next();
// });

var corsOptions = {
    origin: "http://" + appConfig.corsConfig.host + ':' + appConfig.frontEndPort
};
app.all('/*', cors(corsOptions), function (req, res, next) {
    res.render('index.html');
});

if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.json(handler.handleError(404, "Not Found"));
    });
}

app.use(function (err, req, res, next) {
    res.json(handler.handleError(404, "Not Found"));
});

app.listen(port);
console.log('Magic happens at' + appConfig.host + ':' + port);
