var Joi = require('joi');

function validateBookObject(obj) {
    var schema = Joi.object().keys({
        title: Joi.string(),
        description: Joi.string(),
        imageUrl: Joi.string(),
        pdfUrl: Joi.string(),
    });
    return Joi.validate(obj, schema);
}

module.exports = {
    validateBookObject: validateBookObject
};
