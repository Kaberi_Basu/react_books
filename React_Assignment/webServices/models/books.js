var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var timestamps = require('mongoose-timestamp');

var bookSchema = Schema({
    title: { type: String },
    description: { type: String },
    imageUrl: { type: String },
    pdfUrl: { type: String },
    deleted: { type: Boolean, default: false },
});

bookSchema.plugin(timestamps);
module.exports = mongoose.model('Books', bookSchema);
