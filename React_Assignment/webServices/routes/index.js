var books = require('./books');
var cors = require('cors');
var config = require('config');
var appConfig = config.get('Customer.appConfig');


module.exports = function (app) {
    var corsOptions = {
        origin: "http://" + appConfig.corsConfig.host + ":" + appConfig.frontEndPort
    };

    app.use('/api/books', cors(corsOptions), books);

};
