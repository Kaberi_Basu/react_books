var express = require('express');
var booksController = require('../controllers/books');
var router = express.Router();
var helper = require('../helper/response');
//current bank
router.get('/', [booksController.getAllBooks, helper.handleSuccess]);
// router.get('/admin/', [authHelper.isAdmin, bankController.getBankForAdmin, helper.handleSuccess]);
router.post('/', [booksController.addNewBook, helper.handleSuccess]);
// router.put('/:id', [authHelper.isAdmin, bankController.updateBank, helper.handleSuccess]);
router.delete('/:id', [booksController.deleteBook, helper.handleSuccess]);

module.exports = router;
