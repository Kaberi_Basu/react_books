//var Books = require('../models/books');
var helper = require('../helper/response');
var validator = require('validator');
var JoiValidator = require('../helper/joiValidation');
var Books = require('../JsonFiles/books.json');
module.exports = {
    getAllBooks: function (req, res, next) {
        //Books.count({
        //    deleted: false
        //}).exec(function (err, totalCount) {
        //    if (err) {
        //        res.status(422).json(helper.responseObject(422, err, null, true));
        //    } else {
        //        let skip = req.query.skip ? parseInt(req.query.skip) : 0;
        //        let limit = req.query.limit ? parseInt(req.query.limit) : totalCount == 0 ? totalCount + 1 : totalCount;
        //        Books.aggregate([{
        //            '$match': {
        //                deleted: false
        //            }
        //        }, {
        //            '$project': {
        //                title: 1,
        //                description:1,
        //                imageUrl: 1,
        //                pdfUrl: 1,
        //                createdAt: 1
        //            }
        //        }, {
        //            $sort: {
        //                createdAt: -1
        //            }
        //        }]).skip(skip).limit(limit).exec(function (err, books) {
        //            if (err) {
        //                res.status(422).json(helper.responseObject(422, err, null, true));
        //            } else {
        //                let result = {}
        //                result.books = books;
        //                result.totalCount = totalCount;
        //                req.result = result;
        //                next();
        //            }
        //        });
        //    }
        //});
        req.result = Books;
        next();
        //res.sendFile('../JsonFiles/books.json' );
    },

    getBookById: function (req, res, next) {
        if (validator.isMongoId(req.params.id)) {
            Books.findOne({
                _id: req.params.id,
                deleted: false
            })
                .exec(function (err, book) {
                    if (err) {
                        res.status(422).json(helper.responseObject(422, err, null, true));
                    } else {
                        req.result = book;
                        next();
                    }

                });

        } else {
            res.status(404).json(helper.responseObject(404, "Incorrect id format", null, true));
        }
    },

    addNewBook: function (req, res, next) {
        var validate = JoiValidator.validateBookObject(req.body);
        if (validate.error !== null) {
            res.status(422).json(helper.responseObject(422, validate, null, true));
        } else {
            Books.findOne({ title: req.body.title, deleted: false })
                .exec(function (err, book) {
                    console.log("/////////////",book);
                    if (!book) {
                        (new Books(req.body)).save(function (err, book) {
                            if (err)
                                res.status(422).json(helper.responseObject(422, err, null, true));
                            else {
                                req.result = book;
                                next();
                            }
                        });
                    } else {
                        res.status(422).json(helper.responseObject(422, "Book must be unique", null, true));
                    }
                });
        }

    },

    editBook: function (req, res, next) {
            Books.update({
                _id: req.params.id
            }, {
                    $set: {
                        "name.en": req.body.name.en,
                        "description.en": req.body.description.en,
                        "imageUrl": req.body.imageUrl,
                        "lang_state": req.body.lang_state
                    }
                },
                function (err, change) {
                    if (err)
                        res.status(422).json(helper.responseObject(422, err, null, true));
                    if (change.ok === 1 && change.n === 1) {
                        req.result = change;
                        next();
                    } else {
                        res.status(304).json(helper.responseObject(304, "Not modified", null, true));
                    }
                });
    },

    deleteBook: function (req, res, next) {
        Books.update({
            _id: req.params.id
        }, {
                $set: {
                    deleted: true
                }
            }, function (err, change) {
                if (err)
                    res.status(422).json(helper.handleError(422, err));
                if (!(change.ok === 1 && change.n === 1)) {
                    return res.status(422).json(helper.handleError(422, "Module not found sorry"));
                } else {
                    req.result = change;
                    next();
                }
            });
    }
};
